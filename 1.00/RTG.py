﻿from PIL import Image,ImageTk
import math
import Tkinter
import re
import exceptions
import sys
import time

def Img2Matrix(properties):
    global imgWidth
    global imgHeight
    global Matrix
    try:
        img = Image.open(properties[0],'r').convert('L')
    except:
        raw_input("Error while reading object")
        sys.exit(0)
    imgWidth = img.width
    imgHeight = img.height
    values = list(img.getdata())
    print("---INPUT---")
    print(values)
    for i in range(imgHeight):
            Matrix.append(values[0:imgWidth])
            for j in range(imgWidth):
                values.pop(0) 


def Calculate(properties):
    positions = []
    listOfIndexes = []
    finallyValues = []
    listOfXs = []
    accuracy = float(properties[3]) 
    count = 0
    filmLenght = 0
    positions.append([0,0])
    negativePosition = False

    #Následující blok - s určitou přesností(proměnná - accuracy) se vypočítají pozice(pole - positions) mezi filmem a rentgenkou, poté se z obrázku(pole - Matrix) 
    #pomocí pozic výtáhnou hodnoty, které se uloží do pole finallyValues. Potom se vezme další bod na filmu a celé se to opakuje.
    #!!! Blok projede obrázek od středu doprava a potom to stejné z leva do středu. !!!

    while (len(listOfIndexes) == 0 and filmLenght == 0) or (len(listOfIndexes) != 0 and filmLenght != 0):
        horizontalForce = (0 - positions[0][0])/float(accuracy)
        verticalForce = (distanceToObject + imgHeight + distanceToFilm)/float(accuracy)
        listOfIndexes = []
        while positions[-1][1] < distanceToObject + imgHeight:
            #vyhledávání pozice v objektu
            for i in range(imgWidth/2*(-1),imgWidth/2+1):
                if(int(positions[-1][0]) == i):
                    for j in range(imgHeight,0,-1):
                        if(int(positions[-1][1]) == j+distanceToFilm):
                            listOfIndexes.append([i,j-1])
            positions.append([round(positions[-1][0] + horizontalForce,4), round(positions[len(positions)-1][1] + verticalForce,4)])        #určení další pozice
        if(len(listOfIndexes) != 0):
            if(listOfXs.count(positions[0][0]) == 0 and negativePosition == False):
                listOfXs.append(positions[0][0])
                finallyValues.append(0)
            filmLenght += 1
            #přidávání hodnot do finallyValues
            for i in range(1,len(listOfIndexes)-1):
                if(negativePosition):
                    if(imgWidth%2 == 1):
                        finallyValues[positions[0][0]-count-1] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2])
                    else:
                        finallyValues[positions[0][0]-count-1] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2-1])
                else:
                    if(imgWidth%2 == 1):
                        finallyValues[positions[0][0]] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2])
                    else:
                        finallyValues[positions[0][0]] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2-1])
        else:
            #print(finallyValues)
            count = (len(finallyValues)-1) * -1
            positions[0][0] = count
            finallyValues.pop(len(finallyValues)-1)
            negativePosition = True
            listOfIndexes = [0]
            for i in range(len(finallyValues)-1):
                 finallyValues.insert(0,0)

        for i in range(len(positions)-1,0,-1):
            positions.pop(i)
        positions[0][0] += 1

        if(negativePosition and positions[0][0] == 0):
            break
    
    print(finallyValues)
    return finallyValues

def Values2Img(finallyValues):
    rtgImg = Image.new("L",[len(finallyValues),32])
    #protáhnutí obrázku
    for i in range(5):
        finallyValues += finallyValues
    rtgImg.putdata(finallyValues)
    rtgImg.save("output.bmp")
    print("---OUTPUT----")
    img = Image.open("output.bmp",'r').convert('L')
    values = list(img.getdata())
    print(values)
    #zobrazení obrázku
    root = Tkinter.Tk()
    img = ImageTk.PhotoImage(rtgImg)
    Tkinter.Label(root, image=img).pack()
    root.mainloop()

def Scale(finallyValues):
    #pokud jsou všechny hodnoty nulové
    if (max(finallyValues) != 0):
        percent = 255/float(max(finallyValues))
        for i in range(len(finallyValues)-1):
            finallyValues[i] *= float(percent)
    return finallyValues

def GetProperties():
    properties = []
    try:
        file = open("Configuration.ini","r")
        fileData = str(file.readlines())
        imagePath = str(re.findall("-(.*bmp|jpeg|jpg|png|gif)",fileData))
        #vymazání nadbytečných znaků 
        imagePath = imagePath[2:-2]
        for i in range(len(imagePath)-1,1,-1):
            if(imagePath[i] == '\\' and imagePath[i-1] == '\\'):
                imagePath = imagePath[0:i]+imagePath[i+1:]
        properties = list(re.findall("-(\d*\d)",fileData))
        properties.insert(0,imagePath)
    except:
        raw_input("Error while reading configuration file")
        sys.exit(0)

    return properties

distanceToObject = int(GetProperties()[1])
distanceToFilm = int(GetProperties()[2])
Matrix = []

Img2Matrix(GetProperties())
Values2Img(Scale(Calculate(GetProperties())))
