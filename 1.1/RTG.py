from PIL import Image,ImageTk
import math
import Tkinter
import re
import exceptions
import sys

def Img2Matrix(properties):
    global Matrix
    try:
        img = Image.open(properties[0],'r').convert('L') #otevření vstupního obrázku v grayscale hodnotách
    except:
        raw_input("Error while reading image") #error při neúspěšném pokusu o otevření obrázku
        sys.exit(0)
    imgSize = [img.width,img.height] #uložení šířky a výšky obrázku do pole
    values = list(img.getdata()) #získání všech hodnot obrázku
    print("---INPUT---")
    print(values)
     #uložení všech hodnot obrázku(pole) do matice
    for i in range(img.height):
            Matrix.append(values[0:img.width])
            for j in range(img.width):
                values.pop(0)
    VerifyingValues(imgSize,properties) 
    return imgSize

def SetFocus(properties,imgSize):
    focus = float(properties[4])/float(2)*(-1)
    finnalyFocus = float(properties[4])/float(2)
    shiftOfFocus = float(properties[4])/float(5)  #pohyb nulové ohniska
    finnallyValues = []
    print("Focus- " + str(focus))
    finnallyValues = CalculateLeftPart(properties,focus,imgSize) + CalculateRightPart(properties,focus,imgSize) #vypočítání první projekce ke které se poté budou přičítat ostatní
    while(focus != finnalyFocus):
        focus += shiftOfFocus
        print("Focus- " + str(focus))
        values = CalculateLeftPart(properties,focus,imgSize) + CalculateRightPart(properties,focus,imgSize) #vypočítání projekce s aktuálním ohniskem
        #sečtení aktualní projekce s nulovým ohniskem k minulým projekcím
        for i in range(len(finnallyValues)):
            finnallyValues[i] += values[i-1]
    return finnallyValues

def VerifyingValues(imgSize,properties):
        distanceToObject = properties[1]
        distanceToFilm = properties[2]
        accuracy = properties[3]
        focus = properties[4]
        objectHeight = imgSize[1]

        try:
            #zkouška jestli jdou všechny čísla převést na float
            float(distanceToObject)
            float(distanceToFilm)
            float(accuracy)
            float(focus)
        except:
            raw_input("Error while reading cofiguration values")
            sys.exit(0)
        if(float(distanceToObject) < float(distanceToFilm) + objectHeight): #pokud bude objekt moc blízko rentgence 
            raw_input("Error while reading cofiguration values")
            sys.exit(0)
        for value in properties[1:]: #pokud bude některá hodnota menší jak nula
            if(value <= 0):
                raw_input("Error while reading cofiguration values")
                sys.exit(0)

# Spočítá projekci k levé straně obrázku + střed
def CalculateLeftPart(properties,focus,imgSize):
    positions = []
    listOfIndexes = [0]
    finallyValues = []
    imgWidth = imgSize[0]
    imgHeight = imgSize[1]
    distanceToObject = float(properties[1])
    distanceToFilm = float(properties[2])
    accuracy = float(properties[3]) 
    heightOfScene = distanceToObject + imgHeight + distanceToFilm
    positions.append([0,0])
    append = False

    #Následující blok - s určitou přesností(proměnná - accuracy) se vypočítají pozice(pole - positions) mezi filmem a rentgenkou, poté se z obrázku(pole - Matrix) 
    #pomocí pozic výtáhnou hodnoty, které se uloží do pole finallyValues. Potom se vezme další bod na filmu a celé se to opakuje.

    while (len(listOfIndexes) != 0):
        horizontalForce = (focus - positions[0][0])/float(accuracy) #vypočítání o kolik se posune každá pozice horizontálně
        verticalForce = (heightOfScene)/float(accuracy) #vypočítání o kolik se posune každá pozice vertikálně
        listOfIndexes = []
        #počítá pouze do doby kdy je poslední pozice pod nebo v objektu
        while positions[-1][1] < distanceToObject + imgHeight:  
            #vyhledávání pozice v objektu
            for i in range(imgWidth/2*(-1),imgWidth/2+1):
                if(int(positions[-1][0]) == i):
                    for j in range(imgHeight,0,-1):
                        if(int(positions[-1][1]) == j+distanceToFilm):
                            listOfIndexes.append([i,j-1])
            positions.append([positions[-1][0] + horizontalForce, positions[len(positions)-1][1] + verticalForce])        #určení další pozice
        #přídá 0 pro každé vypočet každého bodu na projekci - prom. append aby se přidání provedlo pouze 1 pro jeden bod na projekci
        if(len(listOfIndexes) != 0):
            if(append == False): 
                append = True
                finallyValues.append(0)
            #přidávání hodnot do finallyValues
            for i in range(1,len(listOfIndexes)-1):
                    if(imgWidth%2 == 1):
                        finallyValues[positions[0][0]*(-1)] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2]) #uložení hodnoty do projekce 
                    else:
                        finallyValues[positions[0][0]*(-1)] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2-1])
        positions = [[positions[0][0]-1,positions[0][1]]] #vymaže všechny pozice až na první kterou zmenší o 1
        append = False
    return finallyValues[::-1]  #vrátí správně otočenou projekci

# Spočítá projekci k pravé straně obrázku
# stejné jako předchozí funkce pouze počítá v kladných pozicích
def CalculateRightPart(properties,focus,imgSize):
    positions = []
    listOfIndexes = [0]
    finallyValues = [0]
    imgWidth = imgSize[0]
    imgHeight = imgSize[1]
    distanceToObject = float(properties[1])
    distanceToFilm = float(properties[2])
    accuracy = float(properties[3]) 
    heightOfScene = distanceToObject + imgHeight + distanceToFilm
    positions.append([1,0])
    append = False

    while (len(listOfIndexes) != 0):
        horizontalForce = (focus - positions[0][0])/float(accuracy)
        verticalForce = (heightOfScene)/float(accuracy)
        listOfIndexes = []
        while positions[-1][1] < distanceToObject + imgHeight:
            for i in range(imgWidth/2*(-1),imgWidth/2+1):
                if(int(positions[-1][0]) == i):
                    for j in range(imgHeight,0,-1):
                        if(int(positions[-1][1]) == j+distanceToFilm):
                            listOfIndexes.append([i,j-1])
            positions.append([positions[-1][0] + horizontalForce, positions[len(positions)-1][1] + verticalForce])
        if(len(listOfIndexes) != 0):
            if(append == False): 
                append = True
                finallyValues.append(0)
            for i in range(1,len(listOfIndexes)-1):
                if(imgWidth%2 == 1):
                    finallyValues[positions[0][0]] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2])
                else:
                    finallyValues[positions[0][0]] += float(Matrix[(imgHeight-1) - listOfIndexes[i][1]][listOfIndexes[i][0] + imgWidth/2-1])
        positions = [[positions[0][0]+1,positions[0][1]]]
        append = False

    return finallyValues

def Values2Img(finallyValues):
    rtgImg = Image.new("L",[len(finallyValues),32])
    #protáhnutí obrázku
    for i in range(5):
        finallyValues += finallyValues
    rtgImg.putdata(finallyValues)
    rtgImg.save("output.bmp") #uložení obrázku
    print("---OUTPUT----")
    img = Image.open("output.bmp",'r').convert('L') #otevření outputu v grayscale hodnotách
    values = list(img.getdata()) #uložení grayscale hodnot celého obrázku do values
    print(values)
    #zobrazení obrázku
    root = Tkinter.Tk()
    img = ImageTk.PhotoImage(rtgImg)
    Tkinter.Label(root, image=img).pack()
    root.mainloop()

def Scale(finallyValues):
    #přeškáluje všechny hodnoty na float číslo od 0 do 255
    if (max(finallyValues) != 0):
        percent = 255/float(max(finallyValues))
        for i in range(len(finallyValues)-1):
            finallyValues[i] *= float(percent)
    return finallyValues

def GetProperties():
    properties = []
    try:
        file = open("Configuration.ini","r")
        fileData = str(file.readlines())
        imagePath = str(re.findall("-(.*bmp|jpeg|jpg|png|gif)",fileData)) #regex pro vyhledání obrázku s příponami bmp,jpeg...
        #vymazání nadbytečných znaků 
        imagePath = imagePath[2:-2]
        for i in range(len(imagePath)-1,1,-1):
            if(imagePath[i] == '\\' and imagePath[i-1] == '\\'):
                imagePath = imagePath[0:i]+imagePath[i+1:]
        properties = list(re.findall("-(\d+\.\d+|\d+)",fileData)) #regex pro nalezení všech hodnot vlastností(přesnost...)
        properties.insert(0,imagePath)
    except:
        raw_input("Error while reading configuration file") #vypsání při nepodařeném pokusu otevřít konfig. soubor
        sys.exit(0)
    return properties

Matrix = []
imgSize = Img2Matrix(GetProperties())
Values2Img(Scale(SetFocus(GetProperties(),imgSize)))